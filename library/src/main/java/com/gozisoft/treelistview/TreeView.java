package com.gozisoft.treelistview;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.view.ViewGroup;

/**
 * This interface is the basis for all items displayed within the
 * {@link TreeAdapter}. It contains all the required functionality
 * to create an inheritance hierarchy capable of being used within an adapter.
 */
public interface TreeView {

    /**
     * Gets the layout resource that will be shown as the {@link View} for this Preference.
     *
     * @return The layout resource ID.
     */
    @LayoutRes
    int getLayout();

    /**
     * Gets the position in the list that this TreeView item occupies.
     *
     * @return returns AdapterView.INVALID_POSITION, if not in adapter.
     */
    int getPosition();

    /**
     * The id of the navigation view, usually generated from a view.generateUniqueId();
     */
    long getId();

    /**
     * @return the unique name identifying this object.
     */
    String getKey();

    /**
     * The view built for this viewable.
     *
     * @param convertView
     * @param parent
     * @return
     */
    View getView(View convertView, ViewGroup parent);

    /**
     * @return if true, this item will be in the list.
     */
    boolean isVisible();

    /**
     * If this returns true then the nav view is selectable from
     * the list view and will highlight when pressed.
     *
     * @return boolean result.
     */
    boolean isSelectable();

    /**
     * Manual method for performing a lick operation on an individual cell.
     */
    void performClick();

    /**
     * Called to restore any instance state belonging to a TreeView.
     *
     * @param savedInstanceState the bundle to restore data from.
     */
    Parcelable onRestoreInstanceState(Bundle savedInstanceState);

    /**
     * Called to save any instance state belonging to a TreeView.
     *
     * @param outState the bundle to save state data to.
     */
    Parcelable onSaveInstanceState(Bundle outState);

    /**
     * Called TreeView is added to the list of items within the
     * ListAdapter.
     *
     * @param adapter
     */
    void onAddedToAdapter(TreeAdapter adapter);

    /**
     * Called when the TreeView is removed from the list of items within
     * the ListAdapter.
     *
     * @param adapter
     */
    void onRemovedFromAdapter(TreeAdapter adapter);

    /**
     * Interface definition for a callback to be invoked when a TreeCommon is
     * clicked. Returns true if the click is handled.
     */
    interface OnClickListener {
        boolean onClick(TreeView item);
    }

    /**
     * Interface callback for when a navigation item, of any type, has
     * a visual property changed. This is linked back to the TreeAdapter
     * which is setup as a change listener.
     */
    interface OnNavChangeListener {
        void onNavChanged(TreeView item);
    }
}
