package com.gozisoft.treelistview;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Riad on 24/04/2014.
 */
public class TreeGroup extends TreeNode {

    /**
     * Interface callback for when a navigation item, of any type, has
     * a visual property changed. This is linked back to the TreeAdapter
     * which is setup as a change listener.
     */
    interface OnGroupChangeListener {
        void onNavGroupChanged(TreeGroup group);
    }

    protected OnGroupChangeListener mOnNavGroupChange;

    protected ArrayList<TreeView> mChildren = new ArrayList<>();

    protected boolean mExpanded = true;

    public TreeGroup() {
        super();
    }

    /**
     * Recursive get all child items from the group.
     */
    public void getAllNodes(List<TreeView> list) {
        if (mChildren.isEmpty()) {
            // Small optimisation, prevents the creation of an iterator
            // and therefore an allocation.
            return;
        }

        for (TreeView child : mChildren) {
            // Add the child to the list.
            list.add(child);

            // If the child is a root, get it's children too.
            if (child instanceof TreeGroup) {
                TreeGroup group = (TreeGroup) child;
                group.getAllNodes(list);
            }
        }
    }

    public boolean add(TreeView child) {
        if (mChildren.contains(child)) {
            // Exists
            return true;
        }

        if (mChildren.add(child)) {
            notifyGroupChanged();
            return true;
        } else {
            return false;
        }
    }

    public boolean add(Collection<TreeView> children) {
        if (mChildren.isEmpty()) {
            // Current list is empty, just add all the children.
            if (mChildren.addAll(children)) {
                notifyGroupChanged();
                return true;
            }
        } else {
            int sizeBefore = mChildren.size();
            for (TreeView storedChild : mChildren) {
                for (TreeView newChild : children) {
                    if (!storedChild.equals(newChild)) {
                        mChildren.add(newChild);
                    }
                }
            }

            // Items have been added, notify of change.
            if (sizeBefore < mChildren.size()) {
                notifyGroupChanged();
                return true;
            }
        }

        return false;
    }

    public boolean replace(TreeView child) {
        if (mChildren.contains(child)) {
            // Exists
            return true;
        }

        ListIterator<TreeView> iterator = mChildren.listIterator();
        while (iterator.hasNext()) {
            TreeView internal = iterator.next();
            if (internal.getId() != child.getId()
                    && !internal.getKey().equals(child.getKey())) {
                continue;
            }

            iterator.set(child);
            notifyGroupChanged();
            return true;
        }

        return false;
    }

    public boolean replaceAll(Collection<TreeView> newChildren) {
        // Clear all the children
        mChildren.clear();

        // Add the new children
        if (mChildren.addAll(newChildren)) {
            notifyGroupChanged();
            return true;
        }

        return false;
    }

    public void removeChild(TreeView child) {
        if (!mChildren.contains(child)) {
            return;
        }

        mChildren.remove(child);
        notifyGroupChanged();
    }

    public void clear() {
        mChildren.clear();
        notifyGroupChanged();
    }

    /**
     * Setter for the nav group change listener.
     *
     * @param listener
     */
    public void setOnNavGroupChangeListener(OnGroupChangeListener listener) {
        mOnNavGroupChange = listener;
    }

    protected void notifyGroupChanged() {
        if (mOnNavGroupChange != null) {
            mOnNavGroupChange.onNavGroupChanged(this);
        }
    }
}
