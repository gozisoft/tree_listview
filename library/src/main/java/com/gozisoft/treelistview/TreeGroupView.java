package com.gozisoft.treelistview;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Riad on 03/02/14.
 */
public abstract class TreeGroupView extends TreeGroup implements TreeView {

    // Listener class for clicks
    protected OnClickListener mOnClickListener;

    // Listener class for visual changes
    protected OnNavChangeListener mOnChangeListener;

    protected LayoutInflater mInflater;
    protected Context mContext;
    protected boolean mIsSelectable;
    protected boolean mIsVisible;

    public TreeGroupView(Context context) {
        super();
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mIsSelectable = true;
        mIsVisible = true;
    }

    @Override
    public View getView(View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = onCreateView(parent);
        }
        onBindView(convertView);
        return convertView;
    }

    @Override
    public void performClick() {
        if (mOnClickListener != null) {
            mOnClickListener.onClick(this);
        }
    }

    @Override
    public boolean isVisible() {
        return mIsVisible;
    }

    @Override
    public boolean isSelectable() {
        return mIsSelectable;
    }

    @Override
    public void onAddedToAdapter(TreeAdapter adapter) {
        // Set the on change listener to the adapter.
        // This calls back to the adapter when a property changes
        // within the TreeView.
        mOnChangeListener = adapter;
    }

    @Override
    public void onRemovedFromAdapter(TreeAdapter adapter) {
        // Clear the listener.
        mOnChangeListener = null;
    }

    public Context getContext() {
        return mContext;
    }

    public Resources getResources() {
        return mContext.getResources();
    }

    public LayoutInflater getInflater() {
        return mInflater;
    }

    public void setSelectable(boolean selectable) {
        mIsSelectable = selectable;
    }

    /**
     * Makes a new view to hold the data pointed to by cursor.
     *
     * @param parent The parent to which the new view is attached to
     * @return the newly created view.
     */
    abstract View onCreateView(ViewGroup parent);

    /**
     * Bind an existing view to the data pointed to by cursor
     *
     * @param view Existing view, returned earlier by newView
     *             moved to the correct position.
     */
    abstract void onBindView(View view);
}
