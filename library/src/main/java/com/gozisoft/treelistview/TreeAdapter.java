package com.gozisoft.treelistview;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Riad on 30/01/14.
 * Adapter class for filling list view
 */
public final class TreeAdapter extends BaseAdapter implements TreeGroup.OnGroupChangeListener, TreeView.OnNavChangeListener {

    /**
     * Blocks the class names from being changed anymore.
     */
    private boolean mHasReturnedViewTypeCount = false;
    private volatile boolean mIsSyncing = false;

    /**
     * List of roots
     */
    private List<TreeGroup> mRoots = new ArrayList<>();

    /**
     * A list of items belonging to all NavGroups.
     */
    private List<TreeView> mAllItems = new ArrayList<>();

    /**
     * A list of unique layouts for all mAllItems.
     */
    private List<TreeLayout> mLayouts = new ArrayList<>();

    /**
     * Temp layout for holding a layout during getItemViewType.
     */
    private TreeLayout mTempLayout = new TreeLayout();

    /**
     * Handler to run the sync runnable below on the UI thread.
     */
    private Handler mHandler = new Handler(Looper.getMainLooper());

    private Runnable mSyncRunnable = () -> syncNavItems();

    public TreeAdapter() {
        super();
    }

    public TreeAdapter(List<TreeGroup> roots) {
        super();

        mRoots = roots;
        for (TreeGroup group : mRoots) {
            group.setOnNavGroupChangeListener(this);
        }

        // Get all them der children like!
        syncNavItems();
    }

    public TreeAdapter(TreeGroup group) {
        super();
        add(group);
    }

    @Override
    public int getCount() {
        return mAllItems.size();
    }

    @Override
    public TreeView getItem(int position) {
        return mAllItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mAllItems.get(position).getId();
    }

    @Override
    public int getItemViewType(int position) {
        if (!mHasReturnedViewTypeCount) {
            mHasReturnedViewTypeCount = true;
        }

        final TreeView item = getItem(position);
        mTempLayout = createTreeLayout(item, mTempLayout);

        int viewType = Collections.binarySearch(mLayouts, mTempLayout);
        if (viewType < 0) {
            // This is a class that was seen after we returned the count, so
            // don't recycle it.
            return IGNORE_ITEM_VIEW_TYPE;
        } else {
            return viewType;
        }
    }

    @Override
    public int getViewTypeCount() {
        if (!mHasReturnedViewTypeCount) {
            mHasReturnedViewTypeCount = true;
        }

        return Math.max(1, mLayouts.size());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TreeView item = getItem(position);
        mTempLayout = createTreeLayout(item, mTempLayout);

        if (Collections.binarySearch(mLayouts, mTempLayout) < 0) {
            // Set convertView to null in order to trigger the creation of
            // a tree node
            convertView = null;
        }
        return item.getView(convertView, parent);
    }

    @Override
    public boolean isEnabled(int position) {
        if (position < 0 || position >= getCount()) return true;
        return this.getItem(position).isSelectable();
    }

    @Override
    public void onNavGroupChanged(TreeGroup group) {
        mHandler.removeCallbacks(mSyncRunnable);
        mHandler.post(mSyncRunnable);
    }

    @Override
    public void onNavChanged(TreeView item) {
        notifyDataSetChanged();
    }

    public void add(TreeGroup group) {
        if (!mRoots.contains(group)) {
            // Add a root
            mRoots.add(group);

            // Set a callback from the root to the adapter
            group.setOnNavGroupChangeListener(this);

            // Add the roots children
            syncNavItems();
        }
    }

    /**
     * This will find a root which is used to populate
     * and control what goes into the list.
     *
     * @param key the key to search by.
     * @return the root object you want, or null if not found.
     */
    public TreeGroup findRoot(String key) {
        for (TreeGroup group : mRoots) {
            if (group.getKey().equals(key)) {
                return group;
            }
        }
        return null;
    }

    /**
     * This will search the list of items displayed in the ListView.
     *
     * @param key the key to search by.
     * @return the TreeView in the list, or null if not found.
     */
    public TreeView findListItem(String key) {
        for (TreeView navView : mAllItems) {
            if (navView.getKey().equals(key)) {
                return navView;
            }
        }
        return null;
    }

    /**
     * Clear everything within the adapter. Roots and list items.
     */
    public void clear() {
        mRoots.clear();
        mAllItems.clear();
        mLayouts.clear();
        mTempLayout = new TreeLayout();

        // No more data available.
        notifyDataSetInvalidated();
    }

    public void restoreInstanceState(Bundle savedState) {
        for (TreeView item : mAllItems) {
            item.onRestoreInstanceState(savedState);
        }
    }

    /**
     * Visitor method for saving instance state of items within the adapter.
     *
     * @param outState instance state from Activity/Fragment.
     */
    public void saveInstanceState(Bundle outState) {
        for (TreeView item : mAllItems) {
            item.onSaveInstanceState(outState);
        }
    }

    /**
     * Function sync view from root to this list adapter.
     */
    private void syncNavItems() {
        synchronized (this) {
            if (mIsSyncing) {
                return;
            }
            mIsSyncing = true;
        }

        // Obtain a list of new items
        ArrayList<TreeView> newNavList = new ArrayList<>(mAllItems.size());

        // Get a list of items to display within adapter.
        for (TreeGroup group : mRoots) {
            group.getAllNodes(newNavList);
        }

        // Filter list for visible only.
        for (Iterator<TreeView> itor = newNavList.iterator(); itor.hasNext(); ) {
            if (!itor.next().isVisible()) {
                // Item isn't visible, so remove it from the list.
                itor.remove();
            }
        }

        for (TreeView newNavView : newNavList) {
            if (!mAllItems.contains(newNavView)) {
                // Fire on added function to new items that
                // will be a part of what is displayed in the list.
                newNavView.onAddedToAdapter(this);
            }
        }

        for (TreeView oldNavView : mAllItems) {
            // If the new nav list does not contain a TreeView
            // from the previous list, then the old TreeView is
            // getting removed.
            if (!newNavList.contains(oldNavView)) {
                oldNavView.onRemovedFromAdapter(this);
            }
        }

        for (TreeView newNavView : newNavList) {
            // Create a tree layout for all unique nodes
            final TreeLayout tl = createTreeLayout(newNavView, null);
            if (!mLayouts.contains(tl)) {
                mLayouts.add(tl);
            }
        }

        // Set the list
        mAllItems = newNavList;

        // Update the ListView
        notifyDataSetChanged();

        synchronized (this) {
            mIsSyncing = false;

            // I hate how I have to cast this to a object
            // type just to stop a compiler warning in notifyAll.
            this.notifyAll();
        }
    }

    /**
     * Creates, or recycles, a string that includes the class name and layout id .
     * If a particular Tree type uses 2 different resources, they will be treated as
     * different view types.
     */
    private TreeLayout createTreeLayout(TreeView item, TreeLayout in) {
        TreeLayout nl = in != null ? in : new TreeLayout();
        nl.name = item.getClass().getName();
        nl.resId = item.getLayout();
        return nl;
    }

    private static class TreeLayout implements Comparable<TreeLayout> {
        private int resId;
        private String name;

        public int compareTo(@NonNull TreeLayout other) {
            int compareNames = name.compareTo(other.name);
            if (compareNames == 0) {
                if (resId == other.resId) {
                    return 0;
                } else {
                    return resId - other.resId;
                }
            } else {
                return compareNames;
            }
        }
    }
}
