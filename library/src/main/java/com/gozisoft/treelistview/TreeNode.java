package com.gozisoft.treelistview;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.AbsSavedState;
import android.view.View;
import android.widget.AdapterView;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by Riad on 30/01/14.
 */
public abstract class TreeNode {

    // Used for view ID generation.
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    // Data members
    protected int mPosition;
    protected long mId;
    protected String mKey;

    protected TreeNode() {
        this(generateViewId());
    }

    protected TreeNode(long Id) {
        mPosition = AdapterView.INVALID_POSITION;
        mId = Id;
        mKey = String.valueOf(mId);
    }

    public int getPosition() {
        return mPosition;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getKey() {
        return mKey;
    }

    public void setKey(String key) {
        mKey = key;
    }

    public Parcelable onRestoreInstanceState(Bundle savedInstanceState) {
        Parcelable state = savedInstanceState.getParcelable(mKey);
        if (state != null && state != View.BaseSavedState.EMPTY_STATE) {
            throw new IllegalArgumentException("Wrong state class -- expecting Navigation State");
        }
        return state;
    }

    public Parcelable onSaveInstanceState(Bundle outState) {
        Parcelable state = View.BaseSavedState.EMPTY_STATE;
        if (state != null) {
            outState.putParcelable(mKey, state);
        }
        return state;
    }

    /**
     * A base class for managing the instance state of a navigation item.
     */
    public static class BaseSavedState extends AbsSavedState {
        public BaseSavedState(Parcel source) {
            super(source);
        }

        public BaseSavedState(Parcelable superState) {
            super(superState);
        }

        public static final Creator<BaseSavedState> CREATOR
                = new Creator<BaseSavedState>() {
            public BaseSavedState createFromParcel(Parcel in) {
                return new BaseSavedState(in);
            }

            public BaseSavedState[] newArray(int size) {
                return new BaseSavedState[size];
            }
        };
    }

    /**
     * [Taken from Android source code View.generateViewId]
     * Generate a value suitable for use in setId(int).
     * This value will not collide with ID values generated at build time by aapt for R.id.
     *
     * @return a generated ID value
     */
    public static int generateViewId() {
        for (/**/;/**/ ;/**/) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }
}
