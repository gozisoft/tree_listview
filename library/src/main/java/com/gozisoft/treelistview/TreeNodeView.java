package com.gozisoft.treelistview;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by rgozim on 07/06/2016.
 */

public abstract class TreeNodeView<T extends TreeNodeView<T>> extends TreeNode implements TreeView {

    // Listener class for clicks
    protected OnClickListener mOnClickListener;

    // Listener class for visual changes
    protected OnNavChangeListener mOnChangeListener;

    protected LayoutInflater mInflater;
    protected Context mContext;
    protected boolean mIsSelectable;
    protected boolean mIsVisible;

    protected final Class<? extends T> mSubClass;

    /**
     * Click listener that fires when the user clicks
     * anywhere in the list item view.
     */
    private View.OnClickListener mRootClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            performClick();
        }
    };

    /**
     * Basic constructor.
     *
     * @param context activity context.
     */
    public TreeNodeView(Class<? extends T> subClass, Context context) {
        if (subClass == null) {
            throw new IllegalArgumentException("We need the subclass type.");
        }

        mSubClass = subClass;
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mIsSelectable = true;
        mIsVisible = true;
    }

    @Override
    public View getView(View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = onCreateView(parent);
            convertView.setOnClickListener(mRootClickListener);
        }
        onBindView(convertView);
        return convertView;
    }

    @Override
    public void performClick() {
        if (mOnClickListener != null) {
            mOnClickListener.onClick(this);
        }
    }

    @Override
    public boolean isVisible() {
        return mIsVisible;
    }

    @Override
    public boolean isSelectable() {
        return mIsSelectable;
    }

    @Override
    public void onAddedToAdapter(TreeAdapter adapter) {
        // Set the on change listener to the adapter.
        // This calls back to the adapter when a property changes
        // within the TreeView.
        mOnChangeListener = adapter;
    }

    @Override
    public void onRemovedFromAdapter(TreeAdapter adapter) {
        // Clear the listener.
        mOnChangeListener = null;
    }

    @Override
    public Parcelable onRestoreInstanceState(Bundle savedInstanceState) {
        Parcelable state = super.onRestoreInstanceState(savedInstanceState);
        SavedState savedState = new SavedState(state);
        mIsSelectable = savedState.isSelectable;
        mIsVisible = savedState.isVisible;
        return savedState;
    }

    @Override
    public Parcelable onSaveInstanceState(Bundle outState) {
        final Parcelable superState = super.onSaveInstanceState(outState);
        final SavedState savedState = new SavedState(superState);
        savedState.isSelectable = mIsSelectable;
        savedState.isVisible = mIsVisible;
        return savedState;
    }

    /**
     * Sets the callback to be invoked when this Preference is changed by the
     * user (but before the internal state has been updated).
     *
     * @param listener The callback to be invoked.
     */
    public void setOnClickListener(OnClickListener listener) {
        mOnClickListener = listener;
    }

    public void setSelectable(boolean selectable) {
        mIsSelectable = selectable;
    }

    public Context getContext() {
        return mContext;
    }

    public Resources getResources() {
        return mContext.getResources();
    }

    public LayoutInflater getInflater() {
        return mInflater;
    }

    /**
     * Makes a new view to hold the data pointed to by cursor.
     *
     * @param parent The parent to which the new view is attached to
     * @return the newly created view.
     */
    protected abstract View onCreateView(ViewGroup parent);

    /**
     * Bind an existing view to the data pointed to by cursor
     *
     * @param view Existing view, returned earlier by newView
     *             moved to the correct position.
     */
    protected abstract void onBindView(View view);

    protected void notifyChanged() {
        if (mOnChangeListener != null) {
            mOnChangeListener.onNavChanged(this);
        }
    }

    static class SavedState extends BaseSavedState {
        private boolean isSelectable;
        private boolean isVisible;

        public SavedState(Parcel source) {
            super(source);
            isSelectable = source.readInt() > 0;
            isVisible = source.readInt() > 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            // dest.writeInt(isVisibility.ordinal());
        }

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public static final Creator<SavedState> CREATOR
                = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
